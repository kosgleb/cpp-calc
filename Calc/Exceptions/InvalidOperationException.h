#ifndef CALC_INVALIDOPERATIONEXCEPTION_H
#define CALC_INVALIDOPERATIONEXCEPTION_H

#include <exception>
#include "../Calculator.h"

using namespace Calc;

namespace Calc_Exceptions {
    class InvalidOperationException : public std::exception {
    private:
        CalculatorOperation operation;
    public:
        explicit InvalidOperationException(CalculatorOperation operation);

    public:
        [[nodiscard]] CalculatorOperation getOperation() const;
    };
}


#endif //CALC_INVALIDOPERATIONEXCEPTION_H
