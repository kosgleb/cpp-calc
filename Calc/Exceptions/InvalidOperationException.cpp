#include "InvalidOperationException.h"

using namespace Calc_Exceptions;
using namespace Calc;

InvalidOperationException::InvalidOperationException(CalculatorOperation operation) {
    this->operation = operation;
}

CalculatorOperation InvalidOperationException::getOperation() const {
    return this->operation;
}
