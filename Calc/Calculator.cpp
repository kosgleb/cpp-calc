#include "Calculator.h"
#include "Exceptions/InvalidOperationException.h"

using namespace Calc_Exceptions;

namespace Calc {
    double Calculator::calculate(double firstNumber, double secondNumber, CalculatorOperation operation) {
        switch (operation) {
            case CalculatorOperation::Sum:
                return firstNumber + secondNumber;
            case CalculatorOperation::Sub:
                return firstNumber - secondNumber;
            case CalculatorOperation::Mul:
                return firstNumber * secondNumber;
            case CalculatorOperation::Div:
                if (secondNumber == 0) {
                    throw InvalidOperationException(operation);
                }

                return firstNumber / secondNumber;
            default:
                throw InvalidOperationException(operation);
        }
    }
}