#ifndef CALC_CALCULATOR_H
#define CALC_CALCULATOR_H

namespace Calc {
    enum CalculatorOperation {
        Sum = '+',
        Sub = '-',
        Mul = '*',
        Div = '/',
    };

    class Calculator {
    public:
        static double calculate(double firstNumber, double secondNumber, CalculatorOperation operation);
    };
}


#endif //CALC_CALCULATOR_H
