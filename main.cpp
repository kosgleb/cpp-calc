#include <iostream>
#include "Calc/Calculator.h"
#include "Calc/Exceptions/InvalidOperationException.h"

using namespace std;
using namespace Calc_Exceptions;
using namespace Calc;

int main() {
    double firstNumber, secondNumber;
    char operation;
    int nextAction = 1;

    cout << "This is calculator." << endl;

    while (true) {
        cout
            << "Please, select an action to perform: 1 - go to calc, 0 - exit program" << endl;

        cin >> nextAction;

        if (nextAction == 0) {
            break;
        }

        cout
            << "Please, enter an expression in this format: a+b | a-b | a*b | a/b."
            << endl;

        cout << "Enter first number: " << endl;
        cin >> firstNumber;

        cout << "Enter operation: " << endl;
        cin >> operation;

        cout << "Enter second number: " << endl;
        cin >> secondNumber;

        try {
            cout
                << "Result is: "
                << Calculator::calculate(firstNumber, secondNumber, CalculatorOperation(operation)) << endl;
        } catch (InvalidOperationException &exception) {
            cout << "Invalid operation performed: " << static_cast<char> (exception.getOperation()) << endl;

            return 1;
        }
    }

    return 0;
}
